<?php
// @see: https://gist.github.com/ryanwinchester/578c5b50647df3541794

$ip_check = $_SERVER['REMOTE_ADDR'];
$allow = false;

if (!isset($_GET['tkn']) || $_GET['tkn'] != '0929403835') {
  die("Nope");
}

// "creationDate": "2020-06-11T02:01:22.992123"
$ips_allowed = array(
  '13.52.5.96/28',
  '13.236.8.224/28',
  '18.136.214.96/28',
  '18.184.99.224/28',
  '18.234.32.224/28',
  '52.215.192.224/28',
  '104.192.137.240/28',
  '104.192.138.240/28',
  '104.192.140.240/28',
  '104.192.142.240/28',
  '104.192.143.240/28',
  '185.166.143.240/28',
  '185.166.142.240/28',
  '52.41.219.63/32',
  '34.216.18.129/32',
  '13.236.8.128/25',
  '18.246.31.128/25',
  '34.236.25.177/32',
  '185.166.140.0/22',
  '34.199.54.113/32',
  '35.155.178.254/32',
  '52.204.96.37/32',
  '35.160.177.10/32',
  '52.203.14.55/32',
  '18.184.99.128/25',
  '52.215.192.128/25',
  '104.192.136.0/21',
  '18.205.93.0/27',
  '35.171.175.212/32',
  '18.136.214.0/25',
  '52.202.195.162/32',
  '13.52.5.0/25',
  '34.218.168.212/32',
  '18.234.32.128/25',
  '34.218.156.209/32',
  '52.54.90.98/32',
  '34.232.119.183/32',
  '34.232.25.90/32',
);

function ip_in_range($ip, $range)
{
  list($range, $netmask) = explode('/', $range, 2);
  $ip_decimal = ip2long($ip);
  $range_decimal = ip2long($range);
  $wildcard_decimal = pow(2, (32 - $netmask)) - 1;
  $netmask_decimal = ~$wildcard_decimal;

  return (($ip_decimal & $netmask_decimal) == ($range_decimal & $netmask_decimal));
}

foreach ($ips_allowed as $range) {
  $allow = ip_in_range($ip_check, $range);
  if ($allow) {
    break;
  }
}

if (!$allow) {
  die('IP not allowed: ' . $ip_check);
}

echo "Let's go";
$output = shell_exec('/usr/bin/git pull origin master');
echo $output;
